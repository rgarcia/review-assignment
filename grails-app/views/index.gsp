<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Review Assignment</title>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css" />
</head>

<body>
    <div class="selection">
        <h1>${user}</h1>
        <p class="reviewer">
            <g:if test="${reviewer1}">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAYAAABXuSs3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAc5SURBVGhD7VlZbJRVGJ0YNeKr+kBCMIHOvwyLCyYuL5XOTAsBXBI1VNBEMCibqGFxTQkRH8QNjIBBsQtdaBGQiIkBTXzCFzExmOiD0M50p0hbii3tzH895873l3+m/3QfReOXnDi99/vOd+bec++lNfCfjdaF5hy1at4N8mPOg73YU34cX7RFrcLk4rkqFrY3yVDOozFqb2LPJvSWobFHLGKXqSV3qIaIdVqGch4x9GLPeMQulaGxhfPYtCkNYau1o3CWao7YycYi05SpnMV59GAv9oyFrRZqkKnRB7eqs4gEtvpz4WzFLZSpnEUsam9mL/Zk73HZpTFi7728IEVyESQNYfuUTOUs0OsUe7HnZS4WNMjU6EI9HroRNom16y2zVVMkpJqj9kDrojkzJGXSoxPcTRE7wV7syd71YStOLZIycjRHrPw/8M3jICCJuwIg3iApkx7kdm1CsDc1UIukjBzxiLWLQl0Sgp6Lh63vJWXSg9zumXIhdtklKcOHys+/Hif67HmxiYtmbCFI+rGV0yV10qJpcWg6udnD21M0nKUmSc0erUXW/RdQ4LWJC70CYXu1pE5akDNzhwlqoJa2QvMBSc0e2LIdfiREVxHGI9YJSZ20IKfm9ulJLbgWd0iqf6iSwHW49n5NPQBDScQuve1F1lQpmXCQi5yZNnFBLQ0F9m/UJiVDo7nQmtdeGPK1iQve7djaFVIy4WgstFdk22GCWng1NkfMe6RkaOBQvuW9kvzQrR8l67iUTDjI1Z3FJi5SmqztUjI0sCU/8zDEClCQBdzSeIHd0xW2bpGycUcjOHCmelpoE59eLqipvsA6I2XpEbvPnNMa1h4e8o0zcQmr3lRgrzyDV40v23jA2qaovZJcfj28oCZqi987e67IvRp9H5hvqC0h1fesofpWDY/EGlN1LTcvN8y3z+FGqB8PWNv9lNlDLr8eaYAmre19402RezUS1caPqtRSyfWGSr4AbBgGL4Jovak6FoRUW2SWao+ODaxhrVpnai7fHi6oBZqobaDaSP+9QFUaVrLKTDq1WM13gOcBJA8HBw3bH7VU/YP+2zscWMNacvhxp4FaoElrqzQdpyxoi2ysdrmxRX1pq0QFJquBEinwIxKw6aWnzXELZ+2IwqmBWqgJ2qixH1pFdiAwUGb8oA5ZKeGVQDmwEVgD+BEC3MJ+zMejEMPT7yPQF8hlDWvJ4cetsRqgBmqhJgqnRmjVonsqrKmJA0bCkUmNKmAfQIK18l8faLs8Mja7aJs8PIJN3J7UQC2iS2ukVmiGTfKWDdrEixrgQ4DffB2QSQ6wOW6GMQtnTVbh7MWeOwFqyNCltZYay+nvE4M2yQQLtwNZ/M6tvoIm+I18dHahTZDbj5qsNmEv9vQRTaTsEjwZULW4An0SNA4A3J7XAK6CTyMHV1Ybtn40q86ctofQL9tqswd7pSzhq4laqTmQLAseUV+FlJMlUXvsc+AlwOewarssH51dtE2Q62sTcrMHe3l87QU1quMhrvgR/FM2//pklbFXHbaVOmimTnFmEa+j3QAPTYbf+UhcwfaOaBfOIYe5rPFyaE5ys4dcfWmAJmqjxmS1sYea9c3CSFRYxU6NeUEd9TmoBD23A/DxO4XQAsOtOudaaZNM0QQ5yZ3N19CUrDYvJCqCxSI3PZy9eTPxjb5Tx/AcV2H1vQS0EldjK5AhnlvftWx4u3COOUNsQi5ykjvDrtRALdTUC20iM3vg+d/GA6DqcIK91nHv+k2A57C6dtEi/ewiY1eey7AJOTYDXm6C1kBvfXEcMLeJrNGFU54XdWqsc7w3024dHpxPAV5nnseJglqXwC7zM0QDHONcmmjWkoNcnsOY5AFkzxrj3MD+vKjIGVt078y7DdtURyJV7Tm49CIfCK6YHFZaoPNJf7twjHODNnEfmV2A62uuMnroXlVmLXuLjPGHUxlcl6wx+9Rhj3XY8G1A/M7V7IMVMkW74NzgirOGtV7R4GaPBHpJ28mJ/lJrHoh/4mHRdz7BLX4d4Oph2ymsZXG6XfiZY4MvJXNZw1pw6LuZnDXmaWf/zLul3eSG8960KTi4uwfvfB6oUuBlAA+IgwekszjdLvx8EWOc048Mc1mD2sG7ucr8WJXcfpO0yV0kyoNLsUId6gjufF5jewAcNq5q3yoz/WbBZ47pFeeBZC5qWIsd7HAqjCeE9u8J9VlwBh6Fb/WdX4fVfxeC4F0KbFlkK/xOqcHPWjQtghzmqmNa9Mne3cGc/dl6xFCV5lZ95x/FwcVD4uDGuLg09Ypqm+AzxzjHHObC1yVS/s8GtjucrDPPciXVq4bqfcbkn800+JljnHNqzd+dcqNAyq6N6P7EuDV5yDyo6iB+o6kaC23+fyP9mWP4YjXORxP/I1LOIvGFsdbZZ/YOFIcUwc8YWyPT13Y4X+fdNbDF+mXgFfOM+iZ4pwz/O0Idm3pzEyA//h+pCAT+Amt1iLMb3EpaAAAAAElFTkSuQmCC"
                    alt="${reviewer1.name}" title="${reviewer1.name}" width="40px" height="40px" />
                ${reviewer1.name}
                <span>${reviewer1.reviews}</span>
                <form action="select" method="get">
                    <input type="hidden" name="discard" value="${reviewer1.username},${discardList}">
                    <g:if test="${reviewer2}">
                        <input type="hidden" name="keep" value="${reviewer2.username}">
                    </g:if>
                    <g:if test="${canStillDiscard}">
                        <input class="discard" type="submit" value="discard" />
                    </g:if>
                </form>
            </g:if>
            <g:else>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAYAAABXuSs3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAc5SURBVGhD7VlZbJRVGJ0YNeKr+kBCMIHOvwyLCyYuL5XOTAsBXBI1VNBEMCibqGFxTQkRH8QNjIBBsQtdaBGQiIkBTXzCFzExmOiD0M50p0hbii3tzH895873l3+m/3QfReOXnDi99/vOd+bec++lNfCfjdaF5hy1at4N8mPOg73YU34cX7RFrcLk4rkqFrY3yVDOozFqb2LPJvSWobFHLGKXqSV3qIaIdVqGch4x9GLPeMQulaGxhfPYtCkNYau1o3CWao7YycYi05SpnMV59GAv9oyFrRZqkKnRB7eqs4gEtvpz4WzFLZSpnEUsam9mL/Zk73HZpTFi7728IEVyESQNYfuUTOUs0OsUe7HnZS4WNMjU6EI9HroRNom16y2zVVMkpJqj9kDrojkzJGXSoxPcTRE7wV7syd71YStOLZIycjRHrPw/8M3jICCJuwIg3iApkx7kdm1CsDc1UIukjBzxiLWLQl0Sgp6Lh63vJWXSg9zumXIhdtklKcOHys+/Hif67HmxiYtmbCFI+rGV0yV10qJpcWg6udnD21M0nKUmSc0erUXW/RdQ4LWJC70CYXu1pE5akDNzhwlqoJa2QvMBSc0e2LIdfiREVxHGI9YJSZ20IKfm9ulJLbgWd0iqf6iSwHW49n5NPQBDScQuve1F1lQpmXCQi5yZNnFBLQ0F9m/UJiVDo7nQmtdeGPK1iQve7djaFVIy4WgstFdk22GCWng1NkfMe6RkaOBQvuW9kvzQrR8l67iUTDjI1Z3FJi5SmqztUjI0sCU/8zDEClCQBdzSeIHd0xW2bpGycUcjOHCmelpoE59eLqipvsA6I2XpEbvPnNMa1h4e8o0zcQmr3lRgrzyDV40v23jA2qaovZJcfj28oCZqi987e67IvRp9H5hvqC0h1fesofpWDY/EGlN1LTcvN8y3z+FGqB8PWNv9lNlDLr8eaYAmre19402RezUS1caPqtRSyfWGSr4AbBgGL4Jovak6FoRUW2SWao+ODaxhrVpnai7fHi6oBZqobaDaSP+9QFUaVrLKTDq1WM13gOcBJA8HBw3bH7VU/YP+2zscWMNacvhxp4FaoElrqzQdpyxoi2ysdrmxRX1pq0QFJquBEinwIxKw6aWnzXELZ+2IwqmBWqgJ2qixH1pFdiAwUGb8oA5ZKeGVQDmwEVgD+BEC3MJ+zMejEMPT7yPQF8hlDWvJ4cetsRqgBmqhJgqnRmjVonsqrKmJA0bCkUmNKmAfQIK18l8faLs8Mja7aJs8PIJN3J7UQC2iS2ukVmiGTfKWDdrEixrgQ4DffB2QSQ6wOW6GMQtnTVbh7MWeOwFqyNCltZYay+nvE4M2yQQLtwNZ/M6tvoIm+I18dHahTZDbj5qsNmEv9vQRTaTsEjwZULW4An0SNA4A3J7XAK6CTyMHV1Ybtn40q86ctofQL9tqswd7pSzhq4laqTmQLAseUV+FlJMlUXvsc+AlwOewarssH51dtE2Q62sTcrMHe3l87QU1quMhrvgR/FM2//pklbFXHbaVOmimTnFmEa+j3QAPTYbf+UhcwfaOaBfOIYe5rPFyaE5ys4dcfWmAJmqjxmS1sYea9c3CSFRYxU6NeUEd9TmoBD23A/DxO4XQAsOtOudaaZNM0QQ5yZ3N19CUrDYvJCqCxSI3PZy9eTPxjb5Tx/AcV2H1vQS0EldjK5AhnlvftWx4u3COOUNsQi5ykjvDrtRALdTUC20iM3vg+d/GA6DqcIK91nHv+k2A57C6dtEi/ewiY1eey7AJOTYDXm6C1kBvfXEcMLeJrNGFU54XdWqsc7w3024dHpxPAV5nnseJglqXwC7zM0QDHONcmmjWkoNcnsOY5AFkzxrj3MD+vKjIGVt078y7DdtURyJV7Tm49CIfCK6YHFZaoPNJf7twjHODNnEfmV2A62uuMnroXlVmLXuLjPGHUxlcl6wx+9Rhj3XY8G1A/M7V7IMVMkW74NzgirOGtV7R4GaPBHpJ28mJ/lJrHoh/4mHRdz7BLX4d4Oph2ymsZXG6XfiZY4MvJXNZw1pw6LuZnDXmaWf/zLul3eSG8960KTi4uwfvfB6oUuBlAA+IgwekszjdLvx8EWOc048Mc1mD2sG7ucr8WJXcfpO0yV0kyoNLsUId6gjufF5jewAcNq5q3yoz/WbBZ47pFeeBZC5qWIsd7HAqjCeE9u8J9VlwBh6Fb/WdX4fVfxeC4F0KbFlkK/xOqcHPWjQtghzmqmNa9Mne3cGc/dl6xFCV5lZ95x/FwcVD4uDGuLg09Ypqm+AzxzjHHObC1yVS/s8GtjucrDPPciXVq4bqfcbkn800+JljnHNqzd+dcqNAyq6N6P7EuDV5yDyo6iB+o6kaC23+fyP9mWP4YjXORxP/I1LOIvGFsdbZZ/YOFIcUwc8YWyPT13Y4X+fdNbDF+mXgFfOM+iZ4pwz/O0Idm3pzEyA//h+pCAT+Amt1iLMb3EpaAAAAAElFTkSuQmCC"
                    width="40px" height="40px" />
                No selection
            </g:else>
        </p>
        <p class="reviewer">
            <g:if test="${reviewer2}">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAYAAABXuSs3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAc5SURBVGhD7VlZbJRVGJ0YNeKr+kBCMIHOvwyLCyYuL5XOTAsBXBI1VNBEMCibqGFxTQkRH8QNjIBBsQtdaBGQiIkBTXzCFzExmOiD0M50p0hbii3tzH895873l3+m/3QfReOXnDi99/vOd+bec++lNfCfjdaF5hy1at4N8mPOg73YU34cX7RFrcLk4rkqFrY3yVDOozFqb2LPJvSWobFHLGKXqSV3qIaIdVqGch4x9GLPeMQulaGxhfPYtCkNYau1o3CWao7YycYi05SpnMV59GAv9oyFrRZqkKnRB7eqs4gEtvpz4WzFLZSpnEUsam9mL/Zk73HZpTFi7728IEVyESQNYfuUTOUs0OsUe7HnZS4WNMjU6EI9HroRNom16y2zVVMkpJqj9kDrojkzJGXSoxPcTRE7wV7syd71YStOLZIycjRHrPw/8M3jICCJuwIg3iApkx7kdm1CsDc1UIukjBzxiLWLQl0Sgp6Lh63vJWXSg9zumXIhdtklKcOHys+/Hif67HmxiYtmbCFI+rGV0yV10qJpcWg6udnD21M0nKUmSc0erUXW/RdQ4LWJC70CYXu1pE5akDNzhwlqoJa2QvMBSc0e2LIdfiREVxHGI9YJSZ20IKfm9ulJLbgWd0iqf6iSwHW49n5NPQBDScQuve1F1lQpmXCQi5yZNnFBLQ0F9m/UJiVDo7nQmtdeGPK1iQve7djaFVIy4WgstFdk22GCWng1NkfMe6RkaOBQvuW9kvzQrR8l67iUTDjI1Z3FJi5SmqztUjI0sCU/8zDEClCQBdzSeIHd0xW2bpGycUcjOHCmelpoE59eLqipvsA6I2XpEbvPnNMa1h4e8o0zcQmr3lRgrzyDV40v23jA2qaovZJcfj28oCZqi987e67IvRp9H5hvqC0h1fesofpWDY/EGlN1LTcvN8y3z+FGqB8PWNv9lNlDLr8eaYAmre19402RezUS1caPqtRSyfWGSr4AbBgGL4Jovak6FoRUW2SWao+ODaxhrVpnai7fHi6oBZqobaDaSP+9QFUaVrLKTDq1WM13gOcBJA8HBw3bH7VU/YP+2zscWMNacvhxp4FaoElrqzQdpyxoi2ysdrmxRX1pq0QFJquBEinwIxKw6aWnzXELZ+2IwqmBWqgJ2qixH1pFdiAwUGb8oA5ZKeGVQDmwEVgD+BEC3MJ+zMejEMPT7yPQF8hlDWvJ4cetsRqgBmqhJgqnRmjVonsqrKmJA0bCkUmNKmAfQIK18l8faLs8Mja7aJs8PIJN3J7UQC2iS2ukVmiGTfKWDdrEixrgQ4DffB2QSQ6wOW6GMQtnTVbh7MWeOwFqyNCltZYay+nvE4M2yQQLtwNZ/M6tvoIm+I18dHahTZDbj5qsNmEv9vQRTaTsEjwZULW4An0SNA4A3J7XAK6CTyMHV1Ybtn40q86ctofQL9tqswd7pSzhq4laqTmQLAseUV+FlJMlUXvsc+AlwOewarssH51dtE2Q62sTcrMHe3l87QU1quMhrvgR/FM2//pklbFXHbaVOmimTnFmEa+j3QAPTYbf+UhcwfaOaBfOIYe5rPFyaE5ys4dcfWmAJmqjxmS1sYea9c3CSFRYxU6NeUEd9TmoBD23A/DxO4XQAsOtOudaaZNM0QQ5yZ3N19CUrDYvJCqCxSI3PZy9eTPxjb5Tx/AcV2H1vQS0EldjK5AhnlvftWx4u3COOUNsQi5ykjvDrtRALdTUC20iM3vg+d/GA6DqcIK91nHv+k2A57C6dtEi/ewiY1eey7AJOTYDXm6C1kBvfXEcMLeJrNGFU54XdWqsc7w3024dHpxPAV5nnseJglqXwC7zM0QDHONcmmjWkoNcnsOY5AFkzxrj3MD+vKjIGVt078y7DdtURyJV7Tm49CIfCK6YHFZaoPNJf7twjHODNnEfmV2A62uuMnroXlVmLXuLjPGHUxlcl6wx+9Rhj3XY8G1A/M7V7IMVMkW74NzgirOGtV7R4GaPBHpJ28mJ/lJrHoh/4mHRdz7BLX4d4Oph2ymsZXG6XfiZY4MvJXNZw1pw6LuZnDXmaWf/zLul3eSG8960KTi4uwfvfB6oUuBlAA+IgwekszjdLvx8EWOc048Mc1mD2sG7ucr8WJXcfpO0yV0kyoNLsUId6gjufF5jewAcNq5q3yoz/WbBZ47pFeeBZC5qWIsd7HAqjCeE9u8J9VlwBh6Fb/WdX4fVfxeC4F0KbFlkK/xOqcHPWjQtghzmqmNa9Mne3cGc/dl6xFCV5lZ95x/FwcVD4uDGuLg09Ypqm+AzxzjHHObC1yVS/s8GtjucrDPPciXVq4bqfcbkn800+JljnHNqzd+dcqNAyq6N6P7EuDV5yDyo6iB+o6kaC23+fyP9mWP4YjXORxP/I1LOIvGFsdbZZ/YOFIcUwc8YWyPT13Y4X+fdNbDF+mXgFfOM+iZ4pwz/O0Idm3pzEyA//h+pCAT+Amt1iLMb3EpaAAAAAElFTkSuQmCC"
                    alt="${reviewer2.name}" title="${reviewer2.name}" width="40px" height="40px" />
                ${reviewer2.name}
                <span>${reviewer2.reviews}</span>
                <form action="select" method="get">
                    <input type="hidden" name="discard" value="${reviewer2.username},${discardList}">
                    <g:if test="${reviewer1}">
                        <input type="hidden" name="keep" value="${reviewer1.username}">
                    </g:if>
                    <g:if test="${canStillDiscard}">
                        <input class="discard" type="submit" value="discard" />
                    </g:if>
                </form>
            </g:if>
            <g:else>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAYAAABXuSs3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAc5SURBVGhD7VlZbJRVGJ0YNeKr+kBCMIHOvwyLCyYuL5XOTAsBXBI1VNBEMCibqGFxTQkRH8QNjIBBsQtdaBGQiIkBTXzCFzExmOiD0M50p0hbii3tzH895873l3+m/3QfReOXnDi99/vOd+bec++lNfCfjdaF5hy1at4N8mPOg73YU34cX7RFrcLk4rkqFrY3yVDOozFqb2LPJvSWobFHLGKXqSV3qIaIdVqGch4x9GLPeMQulaGxhfPYtCkNYau1o3CWao7YycYi05SpnMV59GAv9oyFrRZqkKnRB7eqs4gEtvpz4WzFLZSpnEUsam9mL/Zk73HZpTFi7728IEVyESQNYfuUTOUs0OsUe7HnZS4WNMjU6EI9HroRNom16y2zVVMkpJqj9kDrojkzJGXSoxPcTRE7wV7syd71YStOLZIycjRHrPw/8M3jICCJuwIg3iApkx7kdm1CsDc1UIukjBzxiLWLQl0Sgp6Lh63vJWXSg9zumXIhdtklKcOHys+/Hif67HmxiYtmbCFI+rGV0yV10qJpcWg6udnD21M0nKUmSc0erUXW/RdQ4LWJC70CYXu1pE5akDNzhwlqoJa2QvMBSc0e2LIdfiREVxHGI9YJSZ20IKfm9ulJLbgWd0iqf6iSwHW49n5NPQBDScQuve1F1lQpmXCQi5yZNnFBLQ0F9m/UJiVDo7nQmtdeGPK1iQve7djaFVIy4WgstFdk22GCWng1NkfMe6RkaOBQvuW9kvzQrR8l67iUTDjI1Z3FJi5SmqztUjI0sCU/8zDEClCQBdzSeIHd0xW2bpGycUcjOHCmelpoE59eLqipvsA6I2XpEbvPnNMa1h4e8o0zcQmr3lRgrzyDV40v23jA2qaovZJcfj28oCZqi987e67IvRp9H5hvqC0h1fesofpWDY/EGlN1LTcvN8y3z+FGqB8PWNv9lNlDLr8eaYAmre19402RezUS1caPqtRSyfWGSr4AbBgGL4Jovak6FoRUW2SWao+ODaxhrVpnai7fHi6oBZqobaDaSP+9QFUaVrLKTDq1WM13gOcBJA8HBw3bH7VU/YP+2zscWMNacvhxp4FaoElrqzQdpyxoi2ysdrmxRX1pq0QFJquBEinwIxKw6aWnzXELZ+2IwqmBWqgJ2qixH1pFdiAwUGb8oA5ZKeGVQDmwEVgD+BEC3MJ+zMejEMPT7yPQF8hlDWvJ4cetsRqgBmqhJgqnRmjVonsqrKmJA0bCkUmNKmAfQIK18l8faLs8Mja7aJs8PIJN3J7UQC2iS2ukVmiGTfKWDdrEixrgQ4DffB2QSQ6wOW6GMQtnTVbh7MWeOwFqyNCltZYay+nvE4M2yQQLtwNZ/M6tvoIm+I18dHahTZDbj5qsNmEv9vQRTaTsEjwZULW4An0SNA4A3J7XAK6CTyMHV1Ybtn40q86ctofQL9tqswd7pSzhq4laqTmQLAseUV+FlJMlUXvsc+AlwOewarssH51dtE2Q62sTcrMHe3l87QU1quMhrvgR/FM2//pklbFXHbaVOmimTnFmEa+j3QAPTYbf+UhcwfaOaBfOIYe5rPFyaE5ys4dcfWmAJmqjxmS1sYea9c3CSFRYxU6NeUEd9TmoBD23A/DxO4XQAsOtOudaaZNM0QQ5yZ3N19CUrDYvJCqCxSI3PZy9eTPxjb5Tx/AcV2H1vQS0EldjK5AhnlvftWx4u3COOUNsQi5ykjvDrtRALdTUC20iM3vg+d/GA6DqcIK91nHv+k2A57C6dtEi/ewiY1eey7AJOTYDXm6C1kBvfXEcMLeJrNGFU54XdWqsc7w3024dHpxPAV5nnseJglqXwC7zM0QDHONcmmjWkoNcnsOY5AFkzxrj3MD+vKjIGVt078y7DdtURyJV7Tm49CIfCK6YHFZaoPNJf7twjHODNnEfmV2A62uuMnroXlVmLXuLjPGHUxlcl6wx+9Rhj3XY8G1A/M7V7IMVMkW74NzgirOGtV7R4GaPBHpJ28mJ/lJrHoh/4mHRdz7BLX4d4Oph2ymsZXG6XfiZY4MvJXNZw1pw6LuZnDXmaWf/zLul3eSG8960KTi4uwfvfB6oUuBlAA+IgwekszjdLvx8EWOc048Mc1mD2sG7ucr8WJXcfpO0yV0kyoNLsUId6gjufF5jewAcNq5q3yoz/WbBZ47pFeeBZC5qWIsd7HAqjCeE9u8J9VlwBh6Fb/WdX4fVfxeC4F0KbFlkK/xOqcHPWjQtghzmqmNa9Mne3cGc/dl6xFCV5lZ95x/FwcVD4uDGuLg09Ypqm+AzxzjHHObC1yVS/s8GtjucrDPPciXVq4bqfcbkn800+JljnHNqzd+dcqNAyq6N6P7EuDV5yDyo6iB+o6kaC23+fyP9mWP4YjXORxP/I1LOIvGFsdbZZ/YOFIcUwc8YWyPT13Y4X+fdNbDF+mXgFfOM+iZ4pwz/O0Idm3pzEyA//h+pCAT+Amt1iLMb3EpaAAAAAElFTkSuQmCC"
                    width="40px" height="40px" />
                No selection
            </g:else>
        </p>

        <form action="select" method="get">
            <g:if test="${reviewer1 && reviewer2}">
                <input type="text" value="Reviewers: @${reviewer1.username}, @${reviewer2.username}" onclick="this.select()"
                    readonly="readonly" />
            </g:if>
            <g:else>
                <input type="text" value="Please click 'Next reviewers'" readonly="readonly" />
            </g:else>
            <p id="nextReviewers"><input type="submit" value="Next reviewers" /></p>
        </form>
    </div>

</body>

</html>