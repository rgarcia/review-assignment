grails.controllers.defaultScope = 'singleton'

// Server
app.home='/app'
jetty.port=8082
jetty.tmp="${app.home}/tmp"
jetty.host="0.0.0.0"

// Configuration files
grails.config.locations = [
    'classpath:application.properties',
    "file:/app/hidden.properties"
]

// Loggers
log4j = {
    info   'cern.review'
    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
}
