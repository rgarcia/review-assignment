package cern.service.review;

import java.util.PriorityQueue;

/**
 * @author mkhelif
 */
public class PrioritySet<E> extends PriorityQueue<E> {

    @Override
    public boolean offer(E e) {
        if (super.contains(e)) {
            return false;
        }
        return super.offer(e);
    }
}