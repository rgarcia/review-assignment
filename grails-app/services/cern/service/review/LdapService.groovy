package cern.service.review

import java.util.concurrent.TimeUnit

import javax.naming.NamingException
import javax.naming.directory.Attributes

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.ldap.core.AttributesMapper
import org.springframework.ldap.core.LdapTemplate
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct


import com.google.common.base.Supplier
import com.google.common.base.Suppliers


/**
 * Service used to query CERN LDAP service.
 *
 * @author mkhelif
 */
@Service
class LdapService implements AttributesMapper<Reviewer>, Supplier<List<Reviewer>> {

    private final Supplier<List<Reviewer>> members = Suppliers.memoizeWithExpiration(this, 6l, TimeUnit.HOURS)

    @Autowired
    private LdapTemplate ldapTemplate
    
    @Value('${ldap.server.baseDn}')
    private String baseDn

    @Value('${ldap.server.filter_template}')
    private String filter_template

    @Value('${ldap.server.egroup}')
    private String egroup 

    private String filter

    
    @PostConstruct
    public void init() {
        filter = String.format(filter_template, egroup)
    }

    def List<String> queryMembers() {
        return members.get()
    }

    @Override
    public List<Reviewer> get() {
        return ldapTemplate.search(baseDn, filter, this)
    }

    @Override
    Reviewer mapFromAttributes(Attributes attrs) throws NamingException {
        return new Reviewer(
            username: attrs.sAMAccountName.get(0),
            name: attrs.displayName.get(0)
        )
    }
}