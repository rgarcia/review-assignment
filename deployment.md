Deployment Instructions
=======================

Create a PaaS application
-------------------------

Go to https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx  and create a new "PaaS Web Application"

This creates the openshift project associated to your account. We will call the project name PROJECTNAME in the rest of the document.


Create a deployment token (only if non-public repo)
----------------------------------------------------

If your repository needs log-in for getting the docker images, we need to create a deployment token in gitlab.

Go to Settings / Repository / Deploy Tokens and create a new read\_registry token.

Add the token to your openshift project. We call GL\_USER and GL\_TOKEN the values generated in the previous step.
```
$ docker run -it --rm gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
# docker login https://openshift.cern.ch  #Use openshift-dev if your project was in testing, use your login and password
# user=GL_USER
# token=GL_TOKEN
auth=$(echo -n "${user}:${token}" | base64 -w 0)
dockercfg=$(echo "{\"auths\": {\"gitlab-registry.cern.ch\": {\"auth\": \"${auth}\"}, \"gitlab.cern.ch\": {\"auth\": \"${auth}\"}}}")
oc create secret generic gitlab-registry-auth --from-literal=.dockerconfigjson="${dockercfg}"  --type=kubernetes.io/dockerconfigjson

# After this, assign the registry pull secret to the service account running your application.
# Please not that, by default, applications are run by the 'default' service account.
# If that is not your case, adjust the command as appropriate:
oc secrets link default gitlab-registry-auth --for=pull
```


Create an openshift edit user
-----------------------------


Use the docker provided image to create a user in your project

```
$ docker run -it --rm gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
# oc login https://openshift.cern.ch  #Use openshift-dev if your project was in testing, use your login and password

# echo '{"kind":"ServiceAccount","apiVersion":"v1","metadata":{"name":"ci-builder"}}' | oc create -f -
# oc policy add-role-to-user edit --serviceaccount=ci-builder
# oc get sa/ci-builder --template='{{range .secrets}}{{ .name }} {{end}}' | xargs -n 1 oc get secret --template='{{ if .data.token }}{{ .data.token }}{{end}}' | head -n 1 | base64 -d -

```

Note down this token. We will call it OC\_TOKEN and we will add it in CI/CD integrations to be able to perform the update/etc...


Deploy with template
--------------------

```
$ docker run -it -v $PWD:/soft --rm gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
# oc login https://openshift.cern.ch  #Use openshift-dev if your project was in testing, use your login and password
# oc project PROJECTNAME
# oc new-app -f /soft/template/review-assignment.yaml  -p REVIEWER_GROUP=it-dep-db-sas -p AUTHORIZED_GROUPS=it-dep-db-sas
```


Manual alternative instead of template
--------------------------------------

Deploy app

```
$ docker run -it --rm gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
# oc login https://openshift.cern.ch  #Use openshift-dev if your project was in testing, use your login and password
# oc project PROJECTNAME
# oc new-app  --docker-image=gitlab-registry.cern.ch/PATH/PROJECTNAME-assignment:latest --name=PROJECTNAME
```

Go to https://openshift.cern.ch select your project and click on the left on "Add to Project" / Browse Catalog.
Add the cern-sso-proxy item to the project. Do not bind secrets and use as service name the PROJECTNAME

This does not create a persistent volume for the data.



Delete project
--------------

Delete all (except edit user)

```
$ docker run -it --rm gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
# oc login https://openshift.cern.ch  #Use openshift-dev if your project was in testing, use your login and password
# oc project PROJECTNAME
# oc delete all -l app=review-assignment
# oc delete cm --all
# oc delete pvc --all
# oc delete is --all
```



