
package cern.service.review

/*
 * @author jlunadur
 */

import cern.service.review.PrioritySet
import cern.service.review.Reviewer
import cern.service.review.LdapService
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Value
import java.io.File

@Service
class ReviewerService {

    @Value('${csv_path}')
    private final String csvPath

    /**
     * File where scores are saved.
     */
    private File REVIEWERS_FILE 

    /**
     * Queue of reviewers.
     */
    private static final Queue<Reviewer> reviewers = new PrioritySet<>()

    /**
     * Service for querying section members.
     */
    LdapService ldapService

    @PostConstruct
    public void init() {
        REVIEWERS_FILE = new File(csvPath)
        // Load reviewers file
        if (REVIEWERS_FILE.exists()) {
            synchronized (reviewers) {
                REVIEWERS_FILE.splitEachLine(',') { fields ->
                    reviewers.add(new Reviewer(
                        username: fields[0],
                        name: fields[1],
                        reviews: Integer.valueOf(fields[2])
                    ))
                }
            }
        }
    }

    // Add new users from ldap
    public void refreshReviewers() {
        reviewers.addAll(ldapService.queryMembers())
    }

    // Choose a reviewer not in the except username list, discards to be put in ignoredReviewers
    public Reviewer chooseNextReviewer(ArrayList<String> except, Queue<Reviewer> ignoredReviewers) {
        Reviewer newReviewer 
        newReviewer = reviewers.poll()
        while (except.contains(newReviewer.username)) {
            ignoredReviewers.add(newReviewer)
            if (reviewers.isEmpty()) {
                throw Exception("Cannot find valid reviewer")
            }
            newReviewer = reviewers.poll()
        }
        newReviewer.reviews++
        ignoredReviewers.add(newReviewer)
        return newReviewer
    }

    public Reviewer findReviewerByUsername(String username){
        Iterator it = this.reviewers.iterator()
        while (it.hasNext()) {
            Reviewer reviewerMatch = it.next()
            if (reviewerMatch.username.equals(username)) {
                return reviewerMatch
            }
        }
        return null
    }


    // When discarding we need to decrement the score
    public void fixLastDiscardedReviewer(ArrayList<String> except) {
        if (!except.isEmpty() && except.get(0)) {
            Reviewer lastDiscardedReviewer = findReviewerByUsername(except.get(0))
            if (lastDiscardedReviewer && lastDiscardedReviewer.reviews>0) {
                    lastDiscardedReviewer.reviews--
            }
        }
    }

    public Reviewer[] getNextReviewers(ArrayList<String> except, String keep) {
        Reviewer[] nextReviewers = new Reviewer[2]
        Reviewer newReviewer 
        Queue<Reviewer> ignoredReviewers = new PrioritySet<>()

        synchronized (this.reviewers) {
            //Do a -1 to the first discarded person
            fixLastDiscardedReviewer(except)

            //If keep then just find by name and add him to the ignore list
            if(keep) {
               Reviewer keepReviewer = findReviewerByUsername(keep)
               if(!keepReviewer) {
                   throw Exception("Reviewer to keep not found")
               }
               except.add(keep)
               nextReviewers[0] = keepReviewer
            } else {
               nextReviewers[0] = chooseNextReviewer(except, ignoredReviewers)

            }

            // Get the second reviewer
            nextReviewers[1] = chooseNextReviewer(except, ignoredReviewers)
            // Reconstruct the priorityquery list
            this.reviewers.addAll(ignoredReviewers)

            // Save the new reviewers file with updated scores
            REVIEWERS_FILE.withWriter("UTF-8") { writer ->
                this.reviewers.each { reviewer ->
                    writer.write("${reviewer.username},${reviewer.name},${reviewer.reviews}\n")
                }
            }
        }
        return nextReviewers
    }    

    public boolean stillLeft(ArrayList<String> except) {
        if (!except) {
            return true
        }
        return (except.size() + 1) < reviewers.size() 
    }
}

