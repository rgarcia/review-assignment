FROM java:8 as builder
LABEL maintainer="Jose Carlos Luna (Jose.Carlos.Luna@cern.ch)"
COPY . /build/app
RUN set -xe && apt-get -q update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y gettext-base zip \
    && curl -s "https://get.sdkman.io" | bash && bash -c "source /root/.sdkman/bin/sdkman-init.sh && sdk install grails 2.4.4 " \
    && bash -c "source /root/.sdkman/bin/sdkman-init.sh && cd /build/app && sdk u grails 2.4.4 && grails war " 


FROM java:8
WORKDIR /app/data
ENV REVIEWER_GROUP "it-dep-db-sas"
COPY --from=builder /build/app/target/review-assignment.war /
RUN useradd -ms /bin/sh reviewapp && chown -R reviewapp:reviewapp /app && chmod 777 /app
USER reviewapp
EXPOSE 8082
CMD [ "bash", "-c" , "java -Dldap.server.egroup=$REVIEWER_GROUP -jar /review-assignment.war" ]



