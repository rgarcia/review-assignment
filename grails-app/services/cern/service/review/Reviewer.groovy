package cern.service.review

import groovy.transform.Sortable

/**
 * @author mkhelif
 */
class Reviewer implements Comparable<Reviewer> {
    String name
    String username
    int reviews = 0

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Reviewer) {
            return username.equals(obj.username)
        }
        return false
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public int compareTo(Reviewer o) {
        if (this.reviews == o.reviews) {
            return this.username.compareTo(o.username)
        }
        return this.reviews.compareTo(o.reviews)
    }

    @Override
    public String toString() {
        return name;
    }
}