def props = new Properties()

grails.project.class.dir = 'target/classes'
grails.project.test.class.dir = 'target/test-classes'
grails.project.test.reports.dir = 'target/test-reports'

grails.project.dependency.resolver = 'maven'
grails.project.dependency.resolution = {
    inherits('global')

    log 'debug'
    repositories {
        grailsCentral()
        mavenCentral()
    }
    dependencies {
        compile 'com.google.guava:guava:19.0'
        compile 'org.springframework.data:spring-data-commons:1.10.1.RELEASE'
        compile 'org.springframework.ldap:spring-ldap-core:2.0.4.RELEASE'

    }
    plugins {
        build(':tomcat:7.0.55')
        compile ':war-exec:1.0.1'
    }
}
