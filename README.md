# review-assignment

Small application to suggest reviewers using fairness (number of past selections)

The original version was created by Marwan Khelif in IT-CS-CT.

Then some adaptations were added by Jose Carlos Luna IT-DB (be able to discard users) and dockerizing / openshift template

Look at [deployment](deployment.md) instructions


