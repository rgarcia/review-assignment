class UrlMappings {
	static mappings = {
        '/'(controller: 'reviewAssignment', action: 'index', method: 'GET')
        '/select'(controller: 'reviewAssignment', action: 'selectReviewers', method: 'GET') 
	}
}
