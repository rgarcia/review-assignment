package cern.review

import cern.service.review.Reviewer
import cern.service.review.ReviewerService

/**
 * @author mkhelif
 * @author jlunadur
 */
class ReviewAssignmentController {

    ReviewerService reviewerService

    public ReviewAssignmentController() {
    }

    /**
     * Display action.
     */
    def index() {
        render(view: '/index', model: [ 'user': this.authenticatedUser ])
    }

    /**
     * Find next two reviewers using random number and incrementing number.
     */
    def selectReviewers() {
        String keepName = params.keep
        ArrayList<String> discardNames = new ArrayList<String>();

        if (params.discard!=null) {
           discardNames = Arrays.asList(params.discard.split(","))
           discardNames.removeAll(Arrays.asList(null,""))
        }
        discardNames.add(this.authenticatedUser)

        reviewerService.refreshReviewers()

        // Pick two reviewers
        Reviewer[] selectedReviewers
        selectedReviewers = reviewerService.getNextReviewers(discardNames, keepName)

        render(
            view: '/index',
            model: [
                'user': this.authenticatedUser,
                'reviewer1': selectedReviewers[0],
                'reviewer2': selectedReviewers[1],
                'discardList': params.discard,
                'canStillDiscard': reviewerService.stillLeft(discardNames)
            ]
        )
    }

    def getAuthenticatedUser() {
        String authorization = request.getHeader('X-Remote-User')?.chars as String
        return authorization
    }
}
