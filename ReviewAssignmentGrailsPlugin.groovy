class ReviewAssignmentGrailsPlugin {
    def version = "2"
    def grailsVersion = "2.4 > *"
    def pluginExcludes = []

    def title = "Review Assignment"
    def description = 'Review assignment tool'
    def documentation = ''

    def license = "CERN"
    def organization = [ name: "CERN", url: "http://cern.ch/" ]

    def author = "Marwan Khelif"
    def authorEmail = "marwan.khelif@cern.ch"
    def developers = [ [ name: "Marwan Khelif", email: "marwan.khelif@cern.ch" ], [ name: "Jose Carlos Luna", email: "jose.carlos.luna@cern.ch" ] ]
    def issueManagement = [ system: "JIRA", url: "https://its.cern.ch/jira/browse/MISC" ]
    def scm = [ url: "https://gitlab.cern.ch/db" ]
}
